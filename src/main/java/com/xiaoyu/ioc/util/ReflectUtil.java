package com.xiaoyu.ioc.util;

public final class ReflectUtil {

    private ReflectUtil() {
    }
    
    public static Object createInstance(String className) throws Exception {
        Class<?> clazz = Class.forName(className);
        return clazz.newInstance();
    }
    
    public static <T> T createInstance(Class<T> clazz) throws Exception {
        return clazz.newInstance();
    }
}
