package com.xiaoyu.ioc.util;

public final class StringUtil {

    private StringUtil() {
    }
    
    public static boolean isValid(String str) {
        return (str != null) && (!"".equals(str.trim()));
    }
}
