package com.xiaoyu.ioc;

import java.util.Set;

/**
 * container 接口(添加/移除/获取)
 *
 * @version 0.1
 * @author xy
 * @date 2018年6月5日 下午5:00:34
 */
public interface Container {

    /**
     * 初始化
     * @param clazzSet 
     */
    void register(String packageName);
    
    /**
     * 貌似暂时用不到
     */
//    void remove();
    
    /**
     * 根据 class 获取 bean
     * @param clazz
     * @return
     */
    <T> T getByType(Class<T> clazz);
    
    /**
     * 
     * @param name
     * @return
     */
    <T> T getByName(String name);
}
