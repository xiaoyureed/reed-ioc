package com.xiaoyu.ioc.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * put into the Ioc container
 *
 * @version 0.1
 * @author xy
 * @date 2018年6月5日 下午7:48:17
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Bean {

    String value() default "";
}
