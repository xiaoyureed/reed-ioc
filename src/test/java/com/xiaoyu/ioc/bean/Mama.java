package com.xiaoyu.ioc.bean;

import com.xiaoyu.ioc.annotation.Bean;
import com.xiaoyu.ioc.annotation.Inject;

import lombok.Data;

@Bean("mama")
@Data
public class Mama {

    @Inject("son")
    private Son son;
    
    // lombok 提供 getter
}
