package com.xiaoyu.ioc;

import org.junit.Test;

import com.xiaoyu.ioc.impl.DefaultContainer;

public class DefaultContainerTest {

    @Test
    public void testDefaultContainer() {
        DefaultContainer container = DefaultContainer.me();
        container.register("com.xiaoyu.ioc.bean");
        Object oMama = container.getByName("mama");
        System.out.println(oMama);
    }
}
