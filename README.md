# toy-ioc

基于注解的一个ioc容器

## quickstart

```java
@Bean("son")
public class Son {

}

@Bean("mama")
@Data// 需要引入lombok
public class Mama {

    @Inject("son")
    private Son son;
    
    // lombok 提供 getter
}
```

使用:

```java
public class DefaultContainerTest {

    @Test
    public void testDefaultContainer() {
        DefaultContainer container = DefaultContainer.me();
        container.register("com.xiaoyu.ioc.bean");
        Object oMama = container.getByName("mama");
        System.out.println(oMama);
    }
}
```
